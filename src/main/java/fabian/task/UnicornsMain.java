package fabian.task;

import java.util.ArrayList;
import java.util.Collection;

public class UnicornsMain {
    public static void main(String[] args) {
        Collection unicorns = new ArrayList();
        for(int i=0;i<3;i++){
            Unicorn newUnicorn = new Unicorn();
            newUnicorn.setName("Unicorn "+(i+1));
            newUnicorn.setAge(20+i);
            newUnicorn.setCorner_color("Color "+(i+1));
            newUnicorn.setCorner_length(10+i);
            unicorns.add(newUnicorn);
        }
        Unicorn newUnicorn1 = new Unicorn();
        newUnicorn1.setName("Unicorn original");
        newUnicorn1.setAge(100);
        newUnicorn1.setCorner_color("True color");
        newUnicorn1.setCorner_length(9000);
        unicorns.add(newUnicorn1);

        Unicorn newUnicorn2 = new Unicorn();
        newUnicorn2.setName("Unicorn original");
        newUnicorn2.setAge(100);
        newUnicorn2.setCorner_color("True color");
        newUnicorn2.setCorner_length(9000);
        unicorns.add(newUnicorn2);
        for(Object unicorn :unicorns){
            System.out.println(unicorn.toString());
        }
    }
}