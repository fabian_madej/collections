package fabian.task;

import java.util.Objects;

public class Unicorn {
    private static String name;
    private static int age;
    private static int corner_length;
    private static String corner_color;

    public String getName(){
        return name;
    }
    public int getAge(){
        return  age;
    }
    public int getCorner_length(){
        return corner_length;
    }
    public String getCorner_color(){
        return corner_color;
    }

    public void setName(String name){
        this.name=name;
    }
    public void  setAge(int age){
        this.age=age;
    }
    public void setCorner_length(int corner_length){
        this.corner_length=corner_length;
    }
    public void setCorner_color(String corner_color){
        this.corner_color=corner_color;
    }
    public String toString(){
        return name + " " + age + " " + corner_color + " " + corner_length;
    }
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (other == null || getClass() != other.getClass()) {
            return false;
        }
        Unicorn unicorn = (Unicorn) other;
        return age == unicorn.age && Objects.equals(name, unicorn.name);
    }
    public int hashCode() {
        return Objects.hash(name, age);
    }
}